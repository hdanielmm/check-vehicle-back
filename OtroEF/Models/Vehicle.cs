using System.Collections.Generic;

namespace OtroEF.Models
{
    public class Vehicle
    {
        public int Id { get; set; }
        public string LicensePlate { get; set; }
        public string Brand { get; set; }
        public string Line { get; set; }
        public int Model { get; set; }

        public List<VehicleReview> VehicleReviews { get; set; }
    }

}